@extends('templates.main')

@section('content')
<form method="get" action="{{ url('/search') }}" id="frmdata" name="frmdata">
    <div class="row">
        <div class="col col-sm-2">
            <textarea class="form-control" placeholder="Ref.# / OrderID" name ="txtOrderID" id="txtOrderID" rows="3"></textarea> 
        </div>
        <div class="col col-sm-2">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gridRadio" id="rdRef" value="Ref" checked>
                <label class="form-check-label" for="gridRadio1">
                Ref.#
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gridRadio" id="rdOrderID" value="OrderID">
                <label class="form-check-label" for="gridRadio2">
                OrderID
                </label>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col col-sm-2">
            <button type="submit" class="btn btn-primary">Cari Data</button> 
        </div>
    </div>
    <div class="row mt-3">
        <div class="col col-sm-12">
            <table class="table table-bordered" name="tblHasil">
                <thead>
                    <tr class="table-primary">
                        <th>ORDER_SEQ_ID</th>
                        <th>REFERENCE_NUMBER</th>
                        <th>ORD_CREATION_DATE</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($data ?: [] as $row)
                    <tr>
                        <td>{{$row->order_seq_id}}</td>
                        <td>{{$row->reference_number}}</td>
                        <td>{{$row->ord_creation_date}}</td>
                    </tr> 
                    @empty
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr> 
                    @endforelse                    
                </tbody>
            </table>
        </div>
    </div> 
</form>
@endsection