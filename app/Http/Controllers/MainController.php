<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MainController extends Controller
{
    public function index(){
        return view('Master', [
            "title"=> "osmcopydb_ui",
            "data"=> null
        ]);
    }

    public function search(){
        $id = $_GET['txtOrderID'];
        $arrID = preg_split('/\r\n|\r|\n/', $id);
        $type = $_GET['gridRadio'];
        $search_type = "";
        if ($type=="OrderID") {
            // $search_type = "ORDER_SEQ_ID";
            $query = DB::table('OM_ORDER_HEADER_08102021')->whereIn('ORDER_SEQ_ID', $arrID)->get(['ORDER_SEQ_ID', 'REFERENCE_NUMBER', 'ORD_CREATION_DATE']);
        }elseif ($type=="Ref") {
            // $search_type = "REFERENCE_NUMBER";
            $query = DB::table('OM_ORDER_HEADER_08102021')
                    ->where(function($query_where) use($arrID){
                        foreach($arrID as $keyword) {
                            $query_where->orWhere('REFERENCE_NUMBER', 'LIKE', "%$keyword%");
                        }
                    })
                    ->get(['ORDER_SEQ_ID', 'REFERENCE_NUMBER', 'ORD_CREATION_DATE']);
        }
        return view('Master', [
            "title"=> "osmcopydb_ui",
            "data"=> $query
        ]);
    }
}
